XCMDK11D

Digi-Key Part Number	1497-XCMDK11D-ND	
Manufacturer	SunLED
Manufacturer Product Number	XCMDK11D
Description	3MM LC RED LED
Manufacturer Standard Lead Time	8 Weeks
Detailed Description	Red 630nm LED Indication - Discrete 1.75V Radial