2007435-1

Digi-Key Part Number	A141617-ND	
Manufacturer	TE Connectivity AMP Connectors	
Manufacturer Product Number	2007435-1
Description	CONN RCPT HDMI 19POS PCB R/A
Manufacturer Standard Lead Time	33 Weeks
Detailed Description	HDMI - Receptacle Connector 19 Position Panel Mount, Through Hole, Right Angle