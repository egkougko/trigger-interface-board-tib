SN65MLVD204AD

Digi-Key Part Number	296-16669-5-ND	
Manufacturer	Texas Instruments
Manufacturer Product Number	SN65MLVD204AD
Description	IC TRANSCEIVER HALF 1/1 8SOIC
Manufacturer Standard Lead Time	6 Weeks
Detailed Description	1/1 Transceiver Half LVDS, Multipoint 8-SOIC