SN74AHCT1G08DBV3

Digi-Key Part Number	296-SN74AHCT1G08DBV3TR-ND - Tape & Reel (TR)
			296-SN74AHCT1G08DBV3CT-ND - Cut Tape (CT)
			296-SN74AHCT1G08DBV3DKR-ND - Digi-Reel®	
Manufacturer	Texas Instruments	
Manufacturer Product Number	SN74AHCT1G08DBV3	
Description	IC GATE AND 1CH 2-INP SOT23-5
Manufacturer Standard Lead Time	35 Weeks
Detailed Description	AND Gate IC 1 Channel - SOT-23-5