GRM188D71A106KA73D

Digi-Key Part Number	490-14461-2-ND - Tape & Reel (TR)
			490-14461-1-ND - Cut Tape (CT)
			490-14461-6-ND - Digi-Reel®	
Manufacturer	Murata Electronics
Manufacturer Product Number	GRM188D71A106KA73D
Description	CAP CER 10UF 10V X7T 0603
Manufacturer Standard Lead Time	23 Weeks
Detailed Description	10 µF ±10% 10V Ceramic Capacitor X7T 0603 (1608 Metric)