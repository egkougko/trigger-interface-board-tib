RMCF0603FT33K0

Digi-Key Part Number	RMCF0603FT33K0TR-ND - Tape & Reel (TR)
			RMCF0603FT33K0CT-ND - Cut Tape (CT)
			RMCF0603FT33K0DKR-ND - Digi-Reel®
Manufacturer	Stackpole Electronics Inc
Manufacturer Product Number	RMCF0603FT33K0
Description	RES 33K OHM 1% 1/10W 0603
Manufacturer Standard Lead Time	17 Weeks
Detailed Description	33 kOhms ±1% 0.1W, 1/10W Chip Resistor 0603 (1608 Metric) Automotive AEC-Q200 Thick Film