RNCP0603FTD1K00

Digi-Key Part Number	RNCP0603FTD1K00TR-ND - Tape & Reel (TR)
			RNCP0603FTD1K00CT-ND - Cut Tape (CT)
			RNCP0603FTD1K00DKR-ND - Digi-Reel®
Manufacturer	Stackpole Electronics Inc	
Manufacturer Product Number	RNCP0603FTD1K00
Description	RES 1K OHM 1% 1/8W 0603	
Manufacturer Standard Lead Time	13 Weeks
Detailed Description	1 kOhms ±1% 0.125W, 1/8W Chip Resistor 0603 (1608 Metric) Anti-Sulfur Thin Film