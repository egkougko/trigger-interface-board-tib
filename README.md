# Trigger Interface Board - TiB

This project contains the schematics, gerber files, 3D models and C library for the trigger interface board (TiB).
The trigger interface board allows to interface any instrument to the AIDA TLU 1 or 2 while providing additional inputs for external signals to be used for triggering.
An additional trigger output is also provided while the differential part of the board is fully programmable using the provided library.
